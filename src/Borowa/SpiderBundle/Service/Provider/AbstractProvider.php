<?php
/**
 * @author      Adrian Zurek <a.zurek@imerge.pl>
 * @copyright   Copyright (c) 2013 Imerge (http://www.imerge.pl)
 */

namespace Borowa\SpiderBundle\Service\Provider;


abstract class AbstractProvider {
	protected $params;
	protected $name;

	public function __construct(array $params) {
		$this->params = $params;
	}

	abstract protected function collectData($url, $qp);

	protected function spiderUrl($url) {
		$name = 'cache_' . md5($url) . '.html';
		if (!file_exists($name)) {
			$str = file_get_contents($url);
			file_put_contents($name, $str);
		} else {
			$str = file_get_contents($name);
		}
		$qp = htmlqp($str, 'body', array('convert_to_encoding' => 'UTF-8'));

		$data = $this->collectData($url, $qp);
		if (isset($data['next'])) {
			if (!isset($data['ads'])) {
				print_r($data);exit;
			}
			$data['ads'] = array_merge($data['ads'], $this->spiderUrl($data['next']));
		}

		return $data['ads'];
	}

	public function spider($spider) {
		foreach($this->params['urls'] as $url) {
			$spider->send($this->name, $this->spiderUrl($url));
		}
	}
}