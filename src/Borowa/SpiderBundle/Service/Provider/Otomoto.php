<?php
/**
 * @author      Adrian Zurek <a.zurek@imerge.pl>
 * @copyright   Copyright (c) 2013 Imerge (http://www.imerge.pl)
 */

namespace Borowa\SpiderBundle\Service\Provider;


class Otomoto extends AbstractProvider {
	protected $name = 'otomoto';

	protected function parse($article) {
		$title = $article->find('h3')->html();
//			echo $article->html() . "\n--------------------------------------\n";

		$price = $article->find('span.om-price-primary strong')->text();

		$basic = $article->find('p.basic');
		$year = $basic->find('span:first strong')->text();
		$engine = $basic->children('strong')->eq(0);
		$engine = $engine->attr('title') . ' / ' . trim($engine->text());
		$mileage = intval($article->find('span:last strong')->text());

		if ($mileage > 150) {
			return null;
		}
		return array(
			'title' => $title,
			'price' => $price,
			'year' => $year,
			'engine' => $engine,
			'mileage' => $mileage
		);
	}

	public function collectData($url, $qp) {
		$data = array();
		foreach($qp->find('section#omListItems section:not(.promoted) article') as $article) {
			$result = $this->parse($article);
			if ($result) {
				$data['ads'][] = $result;
			}
		}
		if (!isset($data['ads'])) {
			foreach($qp->find('section#omListItems article') as $article) {
				$result = $this->parse($article);
				if ($result) {
					$data['ads'][] = $result;
				}
			}
		}

		$next = $qp->find('p.om-pager a.om-pager-external:last:not(.hidden)');
		if ($next && $next->children('span')->innerHtml() == 'Następna') {
			if (preg_match('#^\?p=(\d+)$#', $next->attr('href'), $matches)) {
				$page = $matches[1];
				$count = 0;
				$url = preg_replace('#p=(\d+)#', 'p=' . $page, $url, 1, $count);
				if (!$count) {
					$url .= '&p=' . $page;
				}
				$data['next'] = $url;
			}
		}

		return $data;
	}
}