<?php
/**
 * @author      Adrian Zurek <a.zurek@imerge.pl>
 * @copyright   Copyright (c) 2013 Imerge (http://www.imerge.pl)
 */

namespace Borowa\SpiderBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Bundle\TwigBundle\TwigEngine;

class Spider extends ContainerAware {
	protected $mailer;
	protected $twig;
	protected $params;

	public function __construct(\Swift_Mailer $mailer, TwigEngine $templating, array $params) {
		$this->mailer = $mailer;
		$this->twig = $templating;
		$this->params = $params;
	}

	public function spider() {
		foreach($this->params['providers'] as $provider) {
			$provider = $this->container->get('spider.' . $provider);
			$provider->spider($this);
		}
	}

	public function send($name, array $data) {
//		$logger = new \Swift_Plugins_Loggers_EchoLogger();
//		$this->mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));
		$message = \Swift_Message::newInstance()
			->setSubject('Ogłoszenia z ' . $name . strftime(' [%d-%m-%Y]'))
			->setFrom($this->params['to'])
			->setTo($this->params['to'])
			->setBody($this->twig->render('BorowaSpiderBundle:Mail:ads.html.twig', array(
				'name' => $name,
				'ads' => $data
			)), 'text/html');
		$this->mailer->send($message);
//		echo 'log';print_r($logger->dump());
//        exit;
	}
}