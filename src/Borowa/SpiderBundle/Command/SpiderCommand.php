<?php
/**
 * @author      Adrian Zurek <a.zurek@imerge.pl>
 * @copyright   Copyright (c) 2013 Imerge (http://www.imerge.pl)
 */

namespace Borowa\SpiderBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SpiderCommand extends ContainerAwareCommand {
	protected function configure() {
		$this->setName('ads:spider')->setDescription('test');
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$this->getContainer()->get('borowa.ads.spider')->spider();
	}
}